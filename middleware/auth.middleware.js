const isAuthenticated = (req, res, next) => {
    if(req.isAuthenticated()) {
      return next();  
    } else {
        //enviar error auth
    }
};

module.exports = {
    isAuthenticated,
}