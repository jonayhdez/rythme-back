const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roomChatsSchema = new Schema (
    {
        users: [{ type: mongoose.Types.ObjectId, ref: 'Users' }],
        messages: { type: Array, require:true }
    },
    {
        timestamps: true
    }
);

const RoomChats = mongoose.model('RoomChats', roomChatsSchema);
module.exports = RoomChats;