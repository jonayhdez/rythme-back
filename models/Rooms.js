const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roomsSchema = new Schema (
    {
        name: { type: String, required: true },
        address: { type: String, required: true },
        image: { type: String, required: true }
    },
    {
        timestamps: true
    }
)

const Rooms = mongoose.model('Rooms', roomsSchema);
module.exports = Rooms;