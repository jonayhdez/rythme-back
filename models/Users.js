const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema(
    {
        name: { type: String, require: true },
        surname: { type: String, require: true },
        dateOfBirth: { type: String, require: true },
        img: { type: String, require: true },
        email: { type: String, require: true },
        password: { type: String, require: true },
        friends: [{
            type: mongoose.Types.ObjectId,
            ref: 'Users'
        }],
        notifications: [{
            type: mongoose.Types.ObjectId,
            ref: 'Notifications'
        }],
        tickets: { type: Array },
        wishList: [{
            type: mongoose.Types.ObjectId,
            ref: 'Events'
        }],
    },
    {
        timestamps: true
    }
);

const Users = mongoose.model('Users', usersSchema);
module.exports = Users;
