const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventsSchema = new Schema (
    {
        singer: {
            type: mongoose.Types.ObjectId,
            ref: 'Singers'
        },
        room: {
            type: mongoose.Types.ObjectId,
            ref: 'Rooms'
        },
        date: { type: String, required: true },
        time: { type: String, required: true },
        price: { type: String, required: true },
        tickets: { type: Number, required: true },
        assistants: { type: Number, required: true }
    },
    {
        timestamps: true
    }
);

const Events = mongoose.model('Events', eventsSchema);
module.exports = Events;