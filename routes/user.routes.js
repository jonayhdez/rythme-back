const express = require('express');
const router = express.Router();
const userController = require('../controllers/users.controller');

router.get('/wishlist', userController.getWishlist);
router.post('/wishlist', userController.postWishlist);
router.post('/save_ticket', userController.saveTicket);
router.post('/save_profile', userController.savePhotoProfile);

module.exports = router;