const express = require('express');
const router = express.Router();
const roomsController = require('../controllers/rooms.controller');

router.get('/', roomsController.getRooms );
router.get('/search', roomsController.getRoomsPerPage);
router.get('/:id', roomsController.getRoomById);


module.exports = router;