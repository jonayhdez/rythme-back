const express = require('express');
const router = express.Router();
const eventsController = require('../controllers/events.controller');

router.get('/', eventsController.getEvents);
router.get('/by-date',eventsController.getEventsByDate);


module.exports = router;