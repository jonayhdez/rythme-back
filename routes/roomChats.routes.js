const express = require('express');
const router = express.Router();
const roomChatsController = require('../controllers/roomChats.controller');

router.get('/', roomChatsController.getRooms);
router.get('/:room', roomChatsController.getMessagesByRoom);


module.exports = router;