const mongoose = require('mongoose'); 
const DB_URL = require('../db.js').DB_URL; 
const Singers = require('../models/Singers.js'); 

const singers = [
    {
        name: 'DAVID GUETTA',
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/guetta_ki4ox7.jpg',
        description: 'Pierre David Guetta (/ˈɡɛtə/, French pronunciation: [pjɛʁ david ɡɛta]; born 7 November 1967) is a French DJ, record producer and songwriter. Globally, he has racked up over 50 million record sales, whereas his total number of streams is over 10 billion.[2] In 2011 & 2020, Guetta was voted as the number one DJ in the DJ Mag Top 100 DJs poll.[3] In 2013, Billboard crowned "When Love Takes Over" as the number one dance-pop collaboration of all time.[4] He has been called the "grandfather of EDM".[5]​',
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "GIGI D'AGOSTINO",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/gigi-dagostino_cv2jjf.jpg',
        description: "Luigino Celestino di Agostino (Italian pronunciation: [luˈiːdʒiːno tʃeˈlɛstiːno di aɡoˈstiːno]; born December 17, 1967), known by his stage name Gigi D'Agostino , is an Italian DJ, remixer, singer and record producer. In 1986, he started his career as a DJ spinning Italo disco. His biggest chart successes include 'Bla Bla Bla', 'Another Way', a cover of Nik Kershaw's 'The Riddle', 'La Passion', 'Super (1, 2, 3)' and 'Lamour toujours', all in the years 1999 and 2000. The hookline of 'Lamour toujours' was also used for the 2018 hit remix/mashup 'In My Mind'.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "HANZ ZIMMER",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/zimmer_xkyrws.jpg',
        description: "Hans Florian Zimmer (German pronunciation: [ˈhans ˈfloːʁi̯aːn ˈtsɪmɐ] (About this soundlisten); born 12 September 1957) is a German film score composer and record producer. Zimmer's works are notable for integrating electronic music sounds with traditional orchestral arrangements. Since the 1980s, he has composed music for over 150 films. His works include The Lion King, for which he won the Academy Award for Best Original Score in 1995, the Pirates of the Caribbean series, Interstellar, Gladiator, Crimson Tide, Inception, Dunkirk, Blade Runner 2049, and The Dark Knight Trilogy. He has received four Grammy Awards, three Classical BRIT Awards, two Golden Globes, and an Academy Award. He was also named on the list of Top 100 Living Geniuses, published by The Daily Telegraph.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "BORIS BREJCHA",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754020/electronica/boris_zsshs9.jpg',
        description: "Boris Brejcha (born November 26, 1981 in Ludwigshafen am Rhein, Rhineland-Palatinate) is a German DJ and record producer. He describes his music style as 'High-Tech Minimal.' He uses the venetian carnival mask as his signature look, being inspired by the Carnival in Rio, at the times when he was performing for the first time, in Brazil. Since then, he has been performing at clubs around the world and at some of the biggest festivals such as Tomorrowland, Timewarp and Exit festival. Boris and his friends Ann Clue and Deniz Bul founded the label 'Fckng Serious' in 2015. In August, 2019, he performed for the first time in Ibiza at Hï Ibiza club.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "TINI",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/tini_jk9hf4.jpg',
        description: "Martina Alejandra Stoessel Muzlera[1] (born 21 March 1997),[2][3] known professionally as Martina Stoessel (Spanish: [ˈtini estoˈesel]), or by the stage name Tini (stylised in all caps) is an Argentine actress, singer, songwriter, dancer and model, who gained international popularity for her debut role as Violetta Castillo in the Disney Channel Latin America original telenovela, Violetta.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "ARMIN VAN BUUREN",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754020/electronica/arminvanburen_gpldli.jpg',
        description: "Armin Jozef Jacobus Daniël van Buuren OON (/væn ˈbjʊərən/ van BEWR-ən;[2] Dutch: [ˈɑrmɪn ˈjoːzəf jɑˈkoːbʏs ˈdaːniˌjɛl vɑn ˈbyːrə(n)]; born 25 December 1976)[1] is a Dutch DJ and record producer from Leiden, South Holland. Since 2001, he has hosted A State of Trance (ASOT), a weekly radio show, which is broadcast to nearly 40 million listeners in 84 countries on over 100 FM radio stations.[3] According to the website DJs and Festivals, 'the radio show propelled him to stardom and helped cultivate an interest in trance music around the world.'",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "ASTRO",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/astro_mbf6zl.jpg',
        description: "Astro (Korean: 아스트로) is a South Korean boy band formed by Fantagio that debuted in 2016.[1] The group is composed of six members: MJ, Jinjin, Cha Eun-woo, Moon Bin, Rocky and Yoon San-ha.[2] They debuted with the single 'Hide & Seek' from their debut EP Spring Up, and were subsequently named by Billboard as one of the best new K-pop groups of 2016.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "TYCHO",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754022/electronica/tycho_umiakr.jpg',
        description: "Scott Hansen (born February 7, 1977), known professionally as Tycho (/ˈtaɪkoʊ/ TY-koh), is an American musician, composer, songwriter and producer based in San Francisco. He is also known as ISO50 for his photographic and design work.[4][5] His music blends multiple stylistic components, including downtempo guitar, analogue synthesis, and ambient elements such as breathing and found sounds of weather broadcasts and dialogue.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "DURAN DURAN",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754020/electronica/duranduran_pwrhqa.jpg',
        description: "Duran Duran (/djʊˌræn djʊˈræn/) are an English new wave[6] band formed in Birmingham in 1978. The group were a leading band in the MTV-driven Second British Invasion of the US in the 1980s.[7] The group was formed by keyboardist Nick Rhodes and bassist John Taylor, with the later addition of drummer Roger Taylor, and after numerous personnel changes, guitarist Andy Taylor (none of the Taylors are related) and lead singer Simon Le Bon. These five members featured the most commercially successful line-up.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "DOJA CAT",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754020/electronica/dojacat_y0qrpk.jpg',
        description: "Amala Ratna Zandile Dlamini (born October 21, 1995), known professionally as Doja Cat, is an American singer, rapper, songwriter, and record producer. Born and raised in Los Angeles, she began making and releasing music on SoundCloud as a teenager. Her song 'So High' caught the attention of Kemosabe and RCA Records where she signed a joint record deal at the age of 17, subsequently releasing her debut EP Purrr! in 2014.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "RONDÒ VENEZZIANO",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754022/electronica/rondo_venezziano_ejufvz.jpg',
        description: "Rondò Veneziano is an Italian chamber orchestra, specializing in Baroque music, playing original instruments, but incorporating a rock-style rhythm section of synthesizer, bass guitar and drums, led by Maestro Gian Piero Reverberi, who is also the principal composer of all of the original Rondò Veneziano pieces. The unusual addition of modern instruments, more suitable for jazz, combined with Reverberi's arrangements and original compositions, have resulted in lavish novel versions of classical works over the years. As a rule in their concert tours, the musicians, mostly women, add to the overall Baroque effect wearing Baroque-era attire and coiffures.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "A-HA",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754020/electronica/aha_ycvjxg.jpg',
        description: "A-ha (usually stylised as a-ha; Norwegian pronunciation: [ɑˈhɑː]) is a Norwegian synth-pop band formed in Oslo in 1982. Founded by Paul Waaktaar-Savoy (guitars), Magne Furuholmen (keyboards, guitars), and Morten Harket (vocals), the band rose to fame during the mid-1980s. A-ha achieved their biggest success with their debut album Hunting High and Low in 1985. That album peaked at number one in their native country Norway, number 2 in the UK, and number 15 on the US Billboard album chart; yielded two international number-one singles: 'Take On Me' and 'The Sun Always Shines on T.V.'; and earned the band a Grammy Award nomination as Best New Artist. In the UK, Hunting High and Low continued its chart success into the following year, becoming one of the best-selling albums of 1986.[2][3] The band released studio albums in 1986, 1988, and 1990, with single hits including 'Hunting High and Low', 'The Living Daylights', 'Stay on These Roads', and 'Crying in the Rain'. In 1994, after their fifth studio album, Memorial Beach (1993), failed to achieve the commercial success of their previous albums, the band went on hiatus.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "MILKY CHANCE",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/milkychance_t9dvnx.jpg',
        description: "Milky Chance is a German rock band originating in Kassel. It consists of vocalist and guitarist Clemens Rehbein, bassist and percussionist Philipp Dausch, and guitarist Antonio Greger. Their first single, 'Stolen Dance', was released in April 2013, topping the charts in several countries. It also won the 1Live Krone radio awards for Best Single.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "LP",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/lp_iunxvn.jpg',
        description: "Laura Pergolizzi (born March 18, 1981), known professionally as LP, is an American singer and songwriter, best known for her single 'Lost on You'. She has released five albums and three EPs. She has written songs for other artists including Cher, Rihanna, the Backstreet Boys, Leona Lewis, Mylène Farmer, Céline Dion and Christina Aguilera.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "PITBULL",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/pitbill_cfqsma.jpg',
        description: "Armando Christian Pérez (born January 15, 1981), known professionally by his stage name Pitbull, is an American rapper, singer, songwriter, brand ambassador, businessman and philanthropist. Pérez began his career in the early 2000s, recording reggaeton, Latin hip hop, and crunk music under a multitude of labels. In 2004, he released his debut album M.I.A.M.I. under TVT Records and the executive production of Lil Jon. Pitbull later released his second album, El Mariel, in 2006 and his third, The Boatlift, in 2007.[8] His fourth album, Rebelution (2009), included his breakthrough hit single 'I Know You Want Me (Calle Ocho)', which peaked at number two on the US Billboard Hot 100 and number four on the UK Singles Chart.",
        style: {
                name: 'Electronica',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613754021/electronica/portada_nqfsdh.jpg'
            },
    },
    {
        name: "NIGHTWISH",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756431/heavymetal/NIGHTWISH_saflba.jpg',
        description: "Nightwish is a symphonic metal band from Kitee, Finland. The band was formed in 1996 by lead songwriter and keyboardist Tuomas Holopainen, guitarist Emppu Vuorinen, and former lead singer Tarja Turunen.[1] The band soon picked up drummer Jukka Nevalainen, and then bassist Sami Vänskä after the release of their debut album, Angels Fall First (1997). In 2001, Vänskä was replaced by Marko Hietala, who also took over the male vocalist role previously filled by Holopainen or guest singers. In January 2021, it was announced that Hietala had left the band.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "SYSTEM OF A DOWN",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/systemofadown_ysabbv.jpg',
        description: "System of a Down (also known as SoaD or simply System) is an Armenian-American heavy metal band formed in Glendale, California, in 1994. It currently consists of members Serj Tankian (lead vocals, keyboards), Daron Malakian (guitar, vocals), Shavo Odadjian (bass, backing vocals), and John Dolmayan (drums), who replaced original drummer Andy Khachaturian in 1997.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "SCORPIONS",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/scorpions_aughi5.jpg',
        description: "Scorpions are a German heavy metal band formed in 1965 in Hanover by Rudolf Schenker.[5] Since the band's inception, its musical style has ranged from hard rock[6][7] to heavy metal.[8][9] The lineup from 1979 to 1992 was the most successful incarnation of the group, and included Klaus Meine (vocals), Rudolf Schenker (rhythm guitar), Matthias Jabs (lead guitar), Francis Buchholz (bass), and Herman Rarebell (drums). The band's only continuous member has been Schenker, although Meine has appeared on all of Scorpions' studio albums, while Jabs has been a consistent member since 1979, and bassist Paweł Mąciwoda and drummer Mikkey Dee have been in the band since 2003 and 2016 respectively.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "SLIPKNOT",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/slipknot_ktx5px.jpg',
        description: "Slipknot is an American heavy metal band formed in Des Moines, Iowa in 1995 by percussionist Shawn Crahan, drummer Joey Jordison and bassist Paul Gray. After several lineup changes in its early years, the band settled on nine members for more than a decade: Crahan, Jordison, Gray, Craig Jones, Mick Thomson, Corey Taylor, Sid Wilson, Chris Fehn, and Jim Root. Gray died on May 24, 2010, and was replaced during 2011–2014 by guitarist Donnie Steele. Jordison was dismissed from the band on December 12, 2013. Steele left during the recording sessions for .5: The Gray Chapter. The band found replacements in Alessandro Venturella on bass and Jay Weinberg on drums. After the departure of Jordison, as of December 2013 the only founding member in the current lineup is percussionist Crahan. Fehn was also dismissed from the band in March 2019 prior to the writing of We Are Not Your Kind.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "RAMMSTEIN",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/rammstein_kh1rwa.jpg',
        description: "Rammstein (German pronunciation: [ˈʁamʃtaɪn]) is a German heavy metal band formed in Berlin in 1994. The band's lineup—consisting of lead vocalist Till Lindemann, lead guitarist Richard Kruspe, rhythm guitarist Paul Landers, bassist Oliver Riedel, drummer Christoph Schneider, and keyboardist Christian 'Flake' Lorenz—has remained unchanged throughout their history. Their approach to songwriting has also remained unchanged, with Lindemann writing and singing the lyrics over instrumental pieces that the rest of the band have completed beforehand. Prior to their formation, some members were associated with the punk rock acts Feeling B and First Arsch.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "MACHINE HEAD",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756431/heavymetal/machinehead_lzzrq7.jpg',
        description: "Machine Head is an American heavy metal band from Oakland, California. The band was formed in 1991 by vocalist/guitarist Robb Flynn and bassist Adam Duce. The band's aggressive musicianship made it one of the pioneering bands in the new wave of American heavy metal, and it is also considered to be part of the second wave of thrash metal bands from the 1990s. Machine Head's current lineup comprises Flynn, bassist Jared MacEachern, guitarist Wacław Kiełtyka and drummer Matt Alston. Duce, guitarists Logan Mader, Ahrue Luster and Phil Demmel, and drummers Tony Costanza, Chris Kontos and Dave McClain are former members of the band; Mader and Kontos are currently touring with the band as part of the 25th anniversary tour for its first album, Burn My Eyes (1994).",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "GUNS N ROSES",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756431/heavymetal/gunsnroses_r61tkl.jpg',
        description: "Guns N' Roses, often abbreviated as GNR, is an American hard rock band from Los Angeles, California, formed in 1985. When they signed to Geffen Records in 1986, the band comprised vocalist Axl Rose, lead guitarist Slash, rhythm guitarist Izzy Stradlin, bassist Duff McKagan, and drummer Steven Adler. The current lineup consists of Rose, Slash, McKagan, guitarist Richard Fortus, drummer Frank Ferrer and keyboardists Dizzy Reed and Melissa Reese.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "METALLICA",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756431/heavymetal/metalicca_ua3dlt.jpg',
        description: "Metallica is an American heavy metal band. The band was formed in 1981 in Los Angeles by vocalist/guitarist James Hetfield and drummer Lars Ulrich, and has been based in San Francisco for most of its career.[1][2] The band's fast tempos, instrumentals and aggressive musicianship made them one of the founding 'big four' bands of thrash metal, alongside Megadeth, Anthrax and Slayer. Metallica's current lineup comprises founding members and primary songwriters Hetfield and Ulrich, longtime lead guitarist Kirk Hammett, and bassist Robert Trujillo. Guitarist Dave Mustaine (who went on to form Megadeth after being fired from the band) and bassists Ron McGovney, Cliff Burton (who died in a bus accident in Sweden in 1986) and Jason Newsted are former members of the band.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "JUDAS PRIEST",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756431/heavymetal/judaspriest_vdadza.jpg',
        description: "Judas Priest are an English heavy metal band formed in Birmingham in 1969. They have sold over 50 million copies of their albums, and are frequently ranked as one of the greatest metal bands of all time. Despite an innovative and pioneering body of work in the latter half of the 1970s, the band had struggled with indifferent record production and a lack of major commercial success until 1980, when they rose to commercial success with the album British Steel.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "FIVE FINGER DEATH PUNCH",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756431/heavymetal/fivefinger_a7tgan.jpg',
        description: "Five Finger Death Punch, also abbreviated as 5FDP or FFDP, is an American heavy metal band from Las Vegas, Nevada, formed in 2005. The band's name comes from kung fu cinema. The band originally consisted of vocalist Ivan Moody, rhythm guitarist Zoltan Bathory, lead guitarist Caleb Andrew Bingham, bassist Matt Snell, and drummer Jeremy Spencer. Bingham was replaced by guitarist Darrell Roberts in 2006, who was then replaced by Jason Hook in 2009. Bassist Matt Snell departed from the band in 2010, and was replaced by Chris Kael in 2011. Spencer then departed the band in 2018 due to recurring back issues, and was replaced by Charlie Engen, making rhythm guitarist Bathory the only remaining founding member of the band. In October 2020, British guitarist Andy James became the band's lead guitarist, replacing Jason Hook.",
        style: {
                name: 'Heavy Metal',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613756432/heavymetal/portada_cffd8g.jpg'
            },
    },
    {
        name: "MIGOS",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758382/hiphop/migos_qtjxom.jpg',
        description: "The Migos /ˈmiːɡoʊs/ are an American hip hop trio from Lawrenceville, Georgia, founded in 2008.[2] They are composed of three rappers known by their stage names Takeoff, Offset, and Quavo. They are managed by Coach K, the former manager of Atlanta-based rappers Gucci Mane and Young Jeezy,[3] and frequently collaborate with producers such as Zaytoven, DJ Durel, Murda Beatz, and Buddah Bless.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "TOBYMAC",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758381/hiphop/tobymac_hkft56.jpg',
        description: "Toby McKeehan (born Kevin Michael McKeehan;[2] October 22, 1964), better known by his stage name TobyMac (styled tobyMac or TOBYMAC), is a Christian hip hop recording artist, music producer, songwriter and author.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "VANILLA ICE",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758381/hiphop/vanillaice_fgegy0.jpg',
        description: "Robert Matthew Van Winkle (born October 31, 1967), known professionally as Vanilla Ice, is an American rapper, actor, and television host. Born in South Dallas, and raised in Texas and South Florida, Ice released his debut album, Hooked, in 1989 on Ichiban Records, before signing a contract with SBK Records, a record label of the EMI Group, which released a reformatted version of the album in 1990 under the title To the Extreme, which contained Ice's best-known hits: 'Ice Ice Baby' and a cover of 'Play That Funky Music'. 'Ice Ice Baby' was the first hip hop single to top the Billboard charts.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "SNOOP DOGG",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758382/hiphop/snoopdog_d26gqg.jpg',
        description: "Calvin Cordozar Broadus Jr. (born October 20, 1971), known professionally as Snoop Dogg (previously Snoop Doggy Dogg and briefly Snoop Lion),[note 1] is an American rapper and media personality. His fame dates to 1992 when he featured on Dr. Dre's debut solo single, 'Deep Cover', and then on Dre's debut solo album, The Chronic. Snoop has since sold over 23 million albums in the United States and 35 million albums worldwide.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "50 CENT",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758381/hiphop/50cent_hec8b7.jpg',
        description: "Curtis James Jackson III (born July 6, 1975),[3] known professionally as 50 Cent, is an American rapper, songwriter, television producer, actor, and entrepreneur. Known for his impact in the hip hop industry, he has been described as a 'master of the nuanced art of lyrical brevity'.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "MERO",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758381/hiphop/mero_f9ro49.jpg',
        description: "Enes Meral (born 28 July 2000), better known by his stage name Mero, is a German rapper with Turkish roots. He rose to prominence in 2018 on Instagram and his debut single 'Baller los', which debuted at the top of the German and Austrian single charts. His follow up singles 'Hobby Hobby' and 'Wolke 10' also topped the German and Austrian charts. In 2019, he was featured in the hits 'Ferrari' by Eno and in 'Kein Plan' by Loredana both topping the German chart. Mero has released three albums, Ya Hero Ya Mero and Unikat in 2019 and Seele in 2020.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "KONTRA K",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758381/hiphop/kontrak_xcfhdk.jpg',
        description: "Kontra K wuchs in Berlin auf. Nach seinem Realschulabschluss entdeckte er mit 16 Jahren die Liebe zum Rap und eiferte seinen US-amerikanischen Idolen Nas, 2Pac, The Notorious B.I.G. und Naughty by Nature nach. Gleichzeitig begann er mit dem Kickboxen. 2006 gründete er mit KiezSpezial das Rapduo Vollkontakt. Das Duo wurde von Kaisa auf dessen Label Hell Raisa Records unter Vertrag genommen. Dort erschien das Fight Club Mixtape Vol.1. Allerdings löste Kaisa anschließend das Label auf. Mit den ehemaligen Labelzugehörigen Skinny Al und Fatal gründete Kontra K anschließend das Label DePeKaRecords. Den Vertrieb übernahm Distributionz.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "DADDY YANKEE",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758381/hiphop/daddyyankee_gx1tqn.jpg',
        description: "Ramón Luis Ayala Rodríguez (born February 3, 1976), known professionally as Daddy Yankee, is a Puerto Rican singer, songwriter, rapper, actor and record producer. Ayala was born in Río Piedras, Puerto Rico, and was raised in the Villa Kennedy Housing Projects neighborhood.[8] Daddy Yankee is the artist who coined the word Reggaeton in 1994 to describe the new music genre that was emerging from Puerto Rico that synthesized hip-hop, Latin Caribbean music, and reggae rhythms with Spanish rapping and singing.[9][10] He is known as the 'King of Reggaetón' by music critics and fans alike. Daddy Yankee is often cited as an influence by other Latin urban performers.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "JASON DERULO",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758381/hiphop/jsonderulo_ee6r7f.jpg',
        description: "Jason Joel Desrouleaux (born September 21, 1989), better known by his stage name Jason Derulo (/dəˈruːloʊ/; formerly stylised as Derülo), is an American singer, songwriter, and dancer.[4][5] Since the start of his solo recording career in 2009, Derulo has sold over 30 million singles and has achieved eleven Platinum singles including 'Wiggle', 'Talk Dirty', 'Want to Want Me', 'Trumpets', 'It Girl', 'In My Head', 'Ridin Solo', and 'Whatcha Say'.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "LIL DURK",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758381/hiphop/lildurk_kkkpkv.jpg',
        description: "Durk Derrick Banks, better known by his stage name Lil Durk (born October 19, 1992), is an American rapper, singer, and songwriter. He is the lead member and founder of his own collective and record label, Only the Family.",
        style: {
                name: 'Hip Hop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613758493/hiphop/portada_xsgzhq.jpg'
            },
    },
    {
        name: "MELENDI",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/melendi_jxbftt.jpg',
        description: "Ramón Melendi Espina (born 21 January 1979), known as Melendi, is a Spanish singer-songwriter. His specialties are rock, flamenco, and rumba styles.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "MARCO ANTONIO SOLIS",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/marcoantoniosolis_d5fpgx.jpg',
        description: "Marco Antonio Solís Sosa (born December 29, 1959) is a Mexican musician, singer, composer, actor, and record producer.Born and raised in Ario de Rosales, Michoacán, Mexico, Solís began his musical career at the age of six, performing part of Los Hermanitos Solís. In 1975 he co-founded Los Bukis, of which he was the lead vocalist and guitarist. The band split up after nearly two decades of success, with Solís pursuing a solo career. Solís released his first solo album, En Pleno Vuelo in 1996 by Fonovisa Records.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "MAELO RUÍZ",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759711/latin/maeloruiz_qflrrm.jpg',
        description: "Ismael Ruiz Hernández (born October 22, 1966), better known as Maelo Ruiz, is a New York City-born Puerto Rican Salsa romántica singer. Maelo Ruiz was born in New York City but at the age of 4 moved with his family to Puerto Rico where his parents were originally from. He started in music at the very young age of 16 when he began to sing in the 'Escuela Libre de Música de Caguas'. But it wasn't until the age of 19 when Maelo Ruiz really started his professional career in music when he became the first voice of Pedro Conga and his International Orchestra.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "GLORIA TREVI",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759711/latin/gloriatrevi_fziys6.jpg',
        description: "Gloria Trevi (Spanish pronunciation: [ˈgloɾja ˈtɾeβi]; born Gloria de los Ángeles Treviño Ruiz on February 15, 1968) is a Mexican singer-songwriter, dancer, actress, television hostess, music video director and businesswoman known as 'The Supreme Diva of Mexican Pop'",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "CHAYANNE",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759711/latin/chayanne_bzez6n.jpg',
        description: "Elmer Figueroa Arce (born June 28, 1968), better known under the stage name Chayanne, is a Puerto Rican Latin pop singer and actor. As a solo artist, Chayanne has released 21 solo albums and sold over 50 million records worldwide,[1] making him one of the best-selling Latin music artists. Chayanne participated in two Puerto Rican telenovelas broadcast by the well-known WAPA-TV television station as for the 80s. These were Sombras del Pasado with Daniel Lugo and Alba Nydia Díaz and Tormento with Daniel Lugo and Yazmin Pereira.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "DAVID BISBAL",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759711/latin/davidbisbal_sjmw6d.jpg',
        description: "David Bisbal Ferre (born 5 June 1979) is a Spanish singer, songwriter, and actor. He gained his initial fame as a runner-up on the interactive reality television show Operación Triunfo. He has since released five studio albums, all of which topped the Spanish Albums Chart, in addition to recording a number of live albums. He has toured throughout Europe and Latin America and is now considered to be a crossover international artist.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "ENRIQUE IGLESIAS",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759711/latin/enriqueiglesias_pf9zrg.jpg',
        description: "Enrique Miguel Iglesias Preysler (Spanish pronunciation: [enˈrike miˈɣel iˈɣlesjas ˈpɾejzleɾ]; born 8 May 1975) is a Spanish singer, songwriter, actor, record producer and philanthropist who is known as the King of Latin Pop.[1][2] He began his career during the mid-1990s on American Spanish-language record label Fonovisa Records under the stage name Enrique Martinez, before switching to his notable surname Iglesias.[3] His father Julio Iglesias was a very successful singer and Iglesias wanted to make it on his own without receiving benefits from his family name. By the turn of the millennium, after becoming one of the biggest stars in Latin America and the Hispanic market in the United States, he made a successful crossover into the US mainstream market. He signed a multi-album deal with Universal Music Group for US$68 million with Universal Music Latino to release his Spanish albums and Interscope Records to release English albums.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "ALEJANDRO FERNÁNDEZ",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759711/latin/alejandrofernandez_fnkdc4.jpg',
        description: "Alejandro Fernández Abarca (Spanish pronunciation: [aleˈxandro feɾˈnandes aˈβaɾka]; born 24 April 1971) is a Mexican singer. Nicknamed as 'El Potrillo' (The Lil’ Colt) by the media and his fans,[1] he has sold over 20 million records worldwide, making him one of the best-selling Latin music artists.[2] Alejandro is the son of the ranchero singer Vicente Fernández.[3] He originally specialized in traditional, earthy forms of Mexican folk music, such as mariachi and ranchera, until he branched out into pop music with great success. Over the course of his career, he has been awarded two Latin Grammy Awards and a star on the Hollywood Walk of Fame.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "ANA GABRIEL",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759711/latin/anagabriel_qr7idw.jpg',
        description: "María Guadalupe Araujo Yong (born December 10, 1955), better known as Ana Gabriel, is a Mexican singer and songwriter from Comanito, Sinaloa, Mexico. She first sang on the stage at age six, singing 'Regalo A Dios' by José Alfredo Jiménez. She moved to Tijuana, Baja California and studied accounting. At age 21, in 1977, she recorded her first song, titled 'Compréndeme'. During her long career, she has hits in three different genres of music: Latin pop, Latin rock, and Mariachi.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "ALEJANDRO SÁNZ",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759711/latin/alejandrosanz_dnhhhz.jpg',
        description: "Alejandro Sánchez Pizarro, better known as Alejandro Sanz (Spanish pronunciation: [aleˈxandɾo ˈsanθ];[1] born December 18, 1968), is a Spanish musician, singer and composer. Sanz has won 22 Latin Grammy Awards and 4 Grammy Awards. He has received the Latin Grammy for Album of the Year three times, more than any other artist. The singer is notable for his flamenco-influenced ballads, and has also experimented with several other genres including pop, rock, funk, R&B and jazz.",
        style: {
                name: 'Latin',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613759712/latin/portada_hesng9.jpg'
            },
    },
    {
        name: "MALUMA",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/maluma_dvlglp.jpg',
        description: "Juan Luis Londoño Arias (born 28 January 1994), known professionally as Maluma, is a Colombian singer, songwriter, and actor signed to Sony Music Colombia, Sony Latin and Fox Music.[1] Musically, Maluma's songs have been described as reggaeton, Latin trap, and pop. Born and raised in Medellín, he developed an interest in music at a young age, and began recording songs at the age of 16. He released his debut album, Magia, a year later in 2012. However, his breakthrough album was 2015's Pretty Boy, Dirty Boy, which led to successful collaborations with several artists. He released F.A.M.E. in 2018, to further commercial success. He followed it up with 11:11 in 2019, and Papi Juancho, released without further announcement in 2020. His single 'Hawái' reached number three on the Billboard Global 200, and became the first number one single on the Billboard Global Excl. U.S. chart. With sales of over 26 million records (albums and singles), Maluma is one of the best-selling Latin music artists.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "PHIL COLLINS",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/philcollins_hhslxi.jpg',
        description: "Philip David Charles Collins LVO (born 30 January 1951) is an English drummer, singer, songwriter, and record producer, best known as the drummer/singer of the rock band Genesis and for his solo career. Between 1982 and 1990, Collins scored three UK and seven US number-one singles in his solo career. When his work with Genesis, his work with other artists, as well as his solo career is totalled, he had more US Top 40 singles than any other artist during the 1980s.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "STING",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/sting_jsrzmi.jpg',
        description: "Gordon Matthew Thomas Sumner CBE (born 2 October 1951), known as Sting, is an English musician, singer, songwriter, and actor. He was the principal songwriter, lead singer, and bassist for new wave rock band The Police from 1977 to 1984, and launched a solo career in 1985. He has included elements of rock, jazz, reggae, classical, new-age and worldbeat in his music.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "HARRY STYLES",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/harrystyles_zbzmcr.jpg',
        description: "Harry Edward Styles (born 1 February 1994) is an English singer, songwriter, and actor. His musical career began in 2010 as a solo contestant on the British music competition series The X Factor. Following his elimination early on, he was brought back to join the boy band One Direction, which went on to become one of the best-selling boy bands of all time.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "DUA LIPA",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762453/pop/dualipa_ymzfjq.jpg',
        description: "Dua Lipa (/ˈduːə ˈliːpə/; Albanian pronunciation: [ˈdua ˈlipa]; born 22 August 1995)[2] is an English singer and songwriter. After working as a model, she signed with Warner Bros. Records in 2014 and released her eponymous debut album in 2017. The album peaked at number three on the UK Albums Chart, and yielded nine singles, including 'Be the One' and 'IDGAF', and the UK number-one single 'New Rules', which also peaked at number six in the US. In 2018, Lipa won the Brit Awards for best British Female Solo Artist and British Breakthrough Act.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "GREEN DAY",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/greenday_o45isa.jpg',
        description: "Dua Lipa (/ˈduːə ˈliːpə/; Albanian pronunciation: [ˈdua ˈlipa]; born 22 August 1995)[2] is an English singer and songwriter. After working as a model, she signed with Warner Bros. Records in 2014 and released her eponymous debut album in 2017. The album peaked at number three on the UK Albums Chart, and yielded nine singles, including 'Be the One' and 'IDGAF', and the UK number-one single 'New Rules', which also peaked at number six in the US. In 2018, Lipa won the Brit Awards for best British Female Solo Artist and British Breakthrough Act.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "COLD PLAY",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762453/pop/coldplay_lzhhpc.jpg',
        description: "Coldplay are a British rock band formed in London in 1996.[1][2] Vocalist, rhythm guitarist and pianist Chris Martin, lead guitarist Jonny Buckland, bassist Guy Berryman, and drummer Will Champion met at University College London and began playing music together from 1996 to 1998, first calling themselves Pectoralz and then Starfish before finally changing their name to Coldplay. Their creative director and former manager Phil Harvey is the fifth member of the band.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "LENNY KRAVITZ",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/lennykravitz_dfom1y.jpg',
        description: "Leonard Albert Kravitz (born May 26, 1964) is an American singer-songwriter, multi-instrumentalist, record producer, and actor. His style incorporates elements of rock, blues, soul, R&B, funk, jazz, reggae, hard rock, psychedelic, pop, folk, and ballads.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "JUSTIN BIEBER",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/lennykravitz_dfom1y.jpg',
        description: "Justin Drew Bieber (/ˈdʒʌstɪn-dru-ˈbiːbər/; born March 1, 1994) is a Canadian singer, songwriter and multi-instrumentalist.[4][5] Discovered at age 13 by talent manager Scooter Braun after he had watched Bieber's YouTube cover song videos, Bieber was signed to RBMG Records in 2008. With Bieber's debut EP My World, released in late 2009, Bieber became the first artist to have seven songs from a debut record chart on the Billboard Hot 100.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },
    {
        name: "AVRIL LAVIGNE",
        image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762453/pop/avril_jlaz9c.jpg',
        description: "Avril Ramona Lavigne (/ˌævrɪl ləˈviːn/ AV-ril lə-VEEN, French: [avʁil laviɲ]; born September 27, 1984)[1] is a Canadian singer, songwriter and actress. By the age of 15, she had appeared on stage with Shania Twain, and by 16, she had signed a two-album recording contract with Arista Records worth more than $2 million.",
        style: {
                name: 'Pop',
                image: 'https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613762454/pop/portada_ut5pbh.jpg'
            },
    },

];


mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true, 
 })
 .then(async () => {
     const allSingers = await Singers.find();
 
     if(allSingers.length) {
         await Singers.collection.drop();
     }
 })
 .catch((err) => {
     console.log(`Error deleting db data ${err}`);
 })
 .then(async () => {
     await Singers.insertMany(singers);
 })
 .catch((err) => {
     console.log(`Error adding data to our db ${err}`)
 })
 .finally(() => mongoose.disconnect());