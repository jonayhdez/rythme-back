const mongoose = require('mongoose'); 
const DB_URL = require('../db.js').DB_URL; 
const Rooms = require('../models/Rooms.js'); 

const rooms = [
    {
        name: "National Music Auditorium",
        address:"Calledel Príncipe de Vergara, 14628002",
        image: "https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613764459/locations/national-music-auditorium_ynhbz3.jpg"
    },
    {
        name: "Teatro Real",
        address:"Plazade Isabel II, s/n28013",
        image: "https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613764460/locations/teatroreal_fachada_j2s8h0.jpg"
    },
    {
        name: "Teatro María Guerrero",
        address:"Callede Tamayo y Baus, 428004",
        image: "https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613764459/locations/teatro_maria_guerrero_4_i0v5tl.jpg"
    },
    {
        name: "Teatro Valle Inclán",
        address:"Plazade Ana Diosdado, s/n28012",
        image: "https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613764459/locations/teatro_valle_inclan_3_wc7wkx.jpg"
    },
    {
        name: "Teatro de la Zarzuela",
        address:"Callede Jovellanos, 428014",
        image: "https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613764459/locations/teatro_de_la_zarzuela_4_cjwqny.jpg"
    },
    {
        name: "Teatros del Canal",
        address:"Callede Cea Bermúdez, 128003",
        image: "https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613764459/locations/teros_del_canal_tsvkpc.jpg"
    },
    {
        name: "Naves del Español en Matadero",
        address:"Paseode la Chopera, 1428045",
        image: "https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613764459/locations/01_matadero_nave1frontal_fgiuvx.jpg"
    },
    {
        name: "Reina Sofia Centre Auditorium 400",
        address:"Rondade Atocha, 228012",
        image: "https://res.cloudinary.com/dj0zfm4sd/image/upload/v1613764459/locations/reinasofia_1425468149.508_ik8jpo.jpg"
    }
];


mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true, 
 })
 .then(async () => {
     const allRooms = await Rooms.find();
 
     if(allRooms.length) {
         await Rooms.collection.drop();
     }
 })
 .catch((err) => {
     console.log(`Error deleting db data ${err}`);
 })
 .then(async () => {
     await Rooms.insertMany(rooms);
 })
 .catch((err) => {
     console.log(`Error adding data to our db ${err}`)
 })
 .finally(() => mongoose.disconnect());
 
