const mongoose = require('mongoose');
const DB_URL = require('../db.js').DB_URL;
const RoomChats = require('../models/RoomChats');

const rooms = [
    {
        users: ['602ebda15af9de375ee69abe','603eab379eac5a3d6534762d'],
        messages: []
    },
    {
        users: ['602ebda15af9de375ee69abe','603eac119eac5a3d6534762e'],
        messages: []
    }
];

mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
.then(async () => {
    const allRooms = await RoomChats.find();

    if (allRooms.length) {
        await RoomChats.drop();
    }
})
.then( async () => {
    await RoomChats.insertMany(rooms);
})
.finally( () => mongoose.disconnect());