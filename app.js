const express = require ('express');
const path = require("path");
require('dotenv').config();

const PORT = process.env.Port || 3200;
const server = express();
const http = require('http').createServer(server);
const router = express.Router();

const session = require('express-session');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const cors = require('cors');

const roomChatsController = require('./controllers/roomChats.controller');

const io = require("socket.io")(http, {
    cors: {
        origin: "*",
    },
});

const NEW_CHAT_MESSAGE_EVENT = "newChatMessage";

io.on("connection", (socket) => {
    // Join a conversation
    const { room } = socket.handshake.query;
    socket.join(room);


    io.in(room).emit('firstLoadMessages', { a:1 });


    // Listen for new messages
    socket.on(NEW_CHAT_MESSAGE_EVENT, (data) => {
        console.log('socket: ',data);
        roomChatsController.postMessagesByRoom(room, data);
        io.in(room).emit(NEW_CHAT_MESSAGE_EVENT, data);
    });


    // Leave the room if the user closes the socket
    socket.on("disconnect", () => {
        socket.leave(room);
    });
});

require('dotenv').config();
require('./passport');

const db = require('./db.js');
db.connect();

const authRoutes = require('./routes/auth.routes');
const userRoutes = require('./routes/user.routes');
const roomRoutes = require('./routes/rooms.routes');
const singersRoutes = require('./routes/singers.routes');
const eventsRoutes = require('./routes/events.routes');
const roomChatsRoutes = require('./routes/roomChats.routes');

server.use((req, res, next) => {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

server.use(cors({ origin: 'http://localhost:3000' }));

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(express.static(path.join(__dirname, 'public')));

server.use(
    session({
        secret: 'RythmeApp',
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 999999999,
        },
        store: new MongoStore({ mongooseConnection: mongoose.connection }),
    })
);

server.use(passport.initialize());
server.use(passport.session());

server.use('/auth', authRoutes);
server.use('/users', userRoutes);
server.use('/rooms', roomRoutes);
server.use('/singers', singersRoutes);
server.use('/events', eventsRoutes);
server.use('/chat', roomChatsRoutes);

server.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});

server.use((err, req, res, next) => {
    // console.log(err)
    return res.status(err.status || 500).json({
        message: err.message || 'Unexpected error',
        status: err.status || 500,
    });
});

server.use('/', router);
server.listen(PORT,() => console.log(`Server started on http://localhost:${PORT}`));
http.listen(3250, () => {
    console.log(`Listening on ${http.address().port}`);
});
