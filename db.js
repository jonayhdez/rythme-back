const mongoose = require('mongoose');
require('dotenv').config(); 
const DB_URL = `mongodb+srv://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASSWORD}@rythme.9mnd5.mongodb.net/rythme-app?retryWrites=true&w=majority` || 'mongodb://localhost:27017/rythme' ; //atlas

const connect = () => {
    mongoose
        .connect(DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        .then(() => {
            console.log("Success connected to DB");
        })
        .catch((error) => {
            console.log("Error connecting to DB: ", error);
        });
}

module.exports = { DB_URL, connect };