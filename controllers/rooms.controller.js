const Rooms = require('../models/Rooms');

const getRooms = async (req, res, next) => {
    try {
        const viewRooms = await Rooms.find();
        
        return res.status(200).json(viewRooms)
    } catch (error) {
        return res.status(401).json(error);
    }
};

const getRoomById = async (req, res, next) => {
    const id = req.params.id;
    try {
        let roomsId = await Rooms.findById(id);
        
        if(!roomsId){
            roomsId = false;
        }
        return res.status(200).json(roomsId);
    } catch (error) {
        return res.status(401).json(error)
    }
};

const getRoomsPerPage = async (req, res, next) => {
    const {order, start, end} = req.query;
    const perPage = end - start + 1
    const skip = start - 1;
    
        return Rooms.find()
        .skip(skip)
        .limit(perPage)
        .then(room =>{
            const result={ result: room }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

module.exports = {
    getRoomById,
    getRooms,
    getRoomsPerPage
}