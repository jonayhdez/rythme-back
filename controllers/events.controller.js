const Events = require('../models/Events');

const getEvents = async (req, res, next) => {
    try {
        const events = await Events.find()
            .sort('date')
            .populate( 'singer')
            .populate('room');

        return res.status(200).json(events)
    } catch (error) {
        next(error);
    }
};

const getEventsByDate = async (req, res, next) => {
    try {
        const events = await Events.find()
            .sort('date')
            .populate( 'singer')
            .populate('room');

        const dataEvent = events.reduce((acc, curr, index) => {
            let newData = [];
            if (acc[curr.date]){
                newData = [...acc[curr.date], curr];
            } else {
                newData = [curr];
            }
            acc[curr.date] = newData;

            return acc;
        }, {})

        return res.status(200).json(dataEvent)
    } catch (error) {
        next(error);
    }
};



module.exports = {
    getEvents,
    getEventsByDate,
    
};