const Singers = require('../models/Singers');

const getSingers = async (req, res, next) => {
    try {
        const viewSingers = await Singers.find();
        return res.status(200).json(viewSingers);
    } catch (error) {
        next(error);
    }
};

const getSingerById = async (req, res, next) => {
    const id = req.params.id;
    try {
        let singersId = await Singers.findById(id);
        return res.status(200).json(singersId)
    } catch (error) {
        next(error)
    }
};

const getSingersPerPage = async(req, res, next) => {
    const {order, start, end} = req.query;
    const perPage = end - start + 1
    const skip = start - 1;
    
        return Singers.find()
        .skip(skip)
        .limit(perPage)
        .then(singer =>{
            const result={ result: singer }
            return res.status(200).json(result);
        })
        .catch(err => {
            next(err);
        })
};

const getStyles = async(req, res, next) => {
    try {
        const styles = await Singers.distinct("style");
        return res.status(200).json(styles)
    } catch (error) {
        next(error);
    }
};

module.exports = {
 getSingers,
 getSingerById,
 getSingersPerPage,
 getStyles,
}