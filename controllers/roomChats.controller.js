const RoomChats = require('../models/RoomChats');

const getRooms = async (req, res, next) => {
  try {
      let message = '';
      let rooms = [];
      if (req.user) {
          rooms = await RoomChats.find({ users: req.user._id }).populate('users');
          message = 'Sent rooms';
      } else {
          message = 'Error, user not logged in';
      }
        console.log(message)
      return res.status(200).json({ data: rooms, message });

  } catch (err) {
      next(err);
  }
};

const getMessagesByRoom = async (req, res, next) => {
    try{
        let message = '';
        let roomData = [];

        if (req.user) {
            const room = req.params.room;
            roomData = await RoomChats.findById(room);
            console.log('room: ', room)
            message = 'Sent messages';
        } else {
            message = 'Error, user not logged in';
        }

        return res.status(200).json({ data: roomData, message });

    } catch (err){
        next(err);
    };
};

const postMessagesByRoom = async (room, message) => {
    try{
        await RoomChats.findByIdAndUpdate(
            room,
            {$push: { messages: message }}
            );
    } catch (err){
        console.log(err);
    }
};

const postRoom = async (req, res, next) => {
    try{
        const users = req.body.users
        const messages = [];
        const newRoom = new RoomChats(
            users,
            messages
        )
    } catch (err) {
        next(err);
    }
};

module.exports = {
    getMessagesByRoom,
    postMessagesByRoom,
    postRoom,
    getRooms
};