const Users = require('../models/Users');
const Events = require('../models/Events');
const Singers = require('../models/Singers');

const saveTicket = async (req, res, next) => {
    try{
        console.log(req.body)
        const { ticket, numTickets, tax } = req.body;
        const userLogged = req.user;
        const idTicket = ticket._id;
        const ticketReport = {
            ticket: ticket,
            numTickets,
            tax
        }
        let message = '';

        if (userLogged){
            await Users.findByIdAndUpdate(
                userLogged._id,
                {$push: {tickets: ticketReport}}
            );

            await Events.findByIdAndUpdate(
                idTicket,
                { $inc: {tickets: -numTickets, assistants: numTickets} }
            );

            message = 'Ticket saved successful';
        } else {
            message = 'Error, the ticket could not be saved'
        }

        return res.status(200).json({ data: idTicket, message });

    } catch (err) {
        next(err);
    }
};

const savePhotoProfile = async (req, res, next) => {
    try{
        const { img } = req.body;
        let message = '';

        if (req.user){
            await Users.findByIdAndUpdate(
                req.user._id,
                { img: img }
            );
            message = 'Image saved';
        } else {
            message = 'Error, user not logged in';
        }

        return res.status(200).json({ message });

    } catch (err) {
        next(err);
    }
};

const getWishlist = async (req, res, next) => {
    const userLogged = req.user;
    if (userLogged){
        try {
            const data = await Users.findById(req.user._id).populate({
                path: 'wishList',
                populate: [ { path: 'singer' },{ path: 'room' }]
                                                                     });
            return res.status(200).json(data);
        } catch (error) {
            next(error)
            }
    }
};

const postWishlist = async (req, res, next) => {
    const userLogged = req.user;
    if (userLogged){ 
    try {

        const {eventId} = req.body;
        const usersId = req.user._id;

        const addWish = await Users.findByIdAndUpdate(
            usersId,
            {$addToSet: {wishList:  eventId}}, //$addToSet si hay un mismo id no lo mete
            {new: true}
        );
        return res.status(200).json(addWish);
    } catch (error) {
        next(error);
        }
    }
}

module.exports = {
    saveTicket,
    getWishlist,
    postWishlist,
    savePhotoProfile
};
